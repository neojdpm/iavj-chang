#include "headers/game_object.h"


GameObject::GameObject() {
	this->kinematic = { glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), 0.0f, 0.0f };
	this->steering = { glm::vec3(0.0f, 0.0f, 0.0f), 0.0f };
}

GameObject::GameObject(glm::vec3 pos, glm::vec3 velocity, GLfloat orientation, GLfloat rotation, glm::vec3 linear, GLfloat angular) {
	this->kinematic = { pos, velocity, orientation, rotation };
	this->steering = { linear, angular };
};

void GameObject::getSteering()
{
	resetSteering();
	std::list<Behaviour*> behaviours = machine.update();
	for (auto behaviour : behaviours) {
		SteeringOutput new_steering = behaviour->getSteering();
		steering.Linear += new_steering.Linear;
		steering.Angular += new_steering.Angular;
	}
}

void GameObject::resetSteering() {
	this->steering.Linear = glm::vec3();
	this->steering.Angular = 0.0f;
}


void GameObject::addState(State* state) {
	this->machine.states.push_back(state);
}

void GameObject::setMesh(Mesh* mesh) {
	this->mesh = mesh;
}

void GameObject::setModel(Model* model) {
	this->model = model;
	is_model = true;
}

void GameObject::jump(float intensity) {
	jumping = true;
	this->kinematic.Velocity.y += 5 * intensity;
}

void GameObject::Draw(Shader* shader, glm::vec3 color)
{
	if (is_model) model->Draw(shader, this->Size, this->kinematic.Position, this->kinematic.Orientation);
	else this->mesh->DrawMesh(this->Size, this->kinematic.Position, this->kinematic.Orientation, color);
	shader->Use();
}

void GameObject::Draw(glm::vec3 color)
{
	this->mesh->DrawMesh(this->Size, this->kinematic.Position, this->kinematic.Orientation, color);
}



std::list<Behaviour*> StateMachine::update() {
	Transition* triggeredTransition = nullptr;

	for (auto transition : this->currentState.getTransitions()) {
		bool TRIGGER = transition.isTriggered();
		if (TRIGGER) {
			triggeredTransition = &transition;
			break;
		}
	}
	State targetState = State();
	if (triggeredTransition) {
		targetState = *triggeredTransition->getTargetState();
		//this->actions = currentState.getEntryAction();
		this->actions = triggeredTransition->getAction();
		//this->actions.push_front(this->targetState.getEntryAction());

		this->currentState = targetState;
		//return this->actions;
		return this->currentState.getAction();
	}
	else return this->currentState.getAction();
}

void State::addAction(Behaviour* behaviour) {
	this->actions.push_back(behaviour);
}
void State::addTransition(Transition Transition) {
	this->transitions.push_back(Transition);
}

