#include "headers/globals.h"
namespace Globals
{
	// forward declarations only
	unsigned int SCR_WIDTH = 1900;
	unsigned int SCR_HEIGHT = 1080;
	bool triangles = false;
	bool drawGraph = false;
	bool paused = true;
	bool debugMode = false;
	unsigned int depthMap;
	unsigned int depthMapFBO;
	std::string map_name = "Floor";
	std::string walls_name = "Walls";
	glm::vec3 gravity = glm::vec3(0.0f, -10.0f, 0.0f);
}
