#include "behavioursDelegated.h"

DynamicSeek::DynamicSeek(GameObject* character, GameObject* target, float maxAcceleration) {
		this->character = character;
		this->target = target;
		this->maxAcceleration = maxAcceleration;
	}

SteeringOutput DynamicSeek::getSteering() {
	SteeringOutput steering = SteeringOutput();

	steering.Linear = glm::normalize(target->kinematic.Position - character->kinematic.Position);
	steering.Linear *= maxAcceleration;

	steering.Angular = 0;
	return steering;
}

DynamicFlee::DynamicFlee(GameObject* character, GameObject* target, float maxAcceleration) {
		this->character = character;
		this->target = target;
		this->maxAcceleration = maxAcceleration;
	}

SteeringOutput DynamicFlee::getSteering() {
	SteeringOutput steering = SteeringOutput();

	steering.Linear = glm::normalize(- target->kinematic.Position + character->kinematic.Position);
	steering.Linear *= maxAcceleration;

	steering.Angular = 0;
	return steering;
}

DynamicArrive::DynamicArrive(GameObject* character, GameObject* target, float maxAcceleration, float maxSpeed, float targetRadius, float slowRadius, float timeToTarget) {
		this->character = character;
		this->target = target;
		this->maxAcceleration = maxAcceleration;
		this->maxSpeed = maxSpeed;
		this->targetRadius = targetRadius;
		this->slowRadius = slowRadius;
		this->timeToTarget = timeToTarget;
	}

SteeringOutput DynamicArrive::getSteering() {
	SteeringOutput steering = SteeringOutput();
	float targetSpeed, distance;
	glm::vec3 direction = target->kinematic.Position - character->kinematic.Position;
	distance = glm::length(direction);

	if (distance < targetRadius) {
		return steering;
	}

	if (distance > slowRadius) {
		targetSpeed = maxSpeed;
	}
	else {
		targetSpeed = maxSpeed * distance / slowRadius;
	}

	glm::vec3 targetVelocity = direction;
		
	targetVelocity = glm::normalize(targetVelocity);
	targetVelocity *= targetSpeed;


	steering.Linear = targetVelocity - character->kinematic.Velocity;
	steering.Linear /= timeToTarget;


	if (glm::length(steering.Linear) > maxAcceleration) {
		steering.Linear = glm::normalize(steering.Linear);
		steering.Linear *= maxAcceleration;
	}

	steering.Angular = 0;
	return steering;
}

DynamicVelocityMatching::DynamicVelocityMatching(GameObject* character, GameObject* target, float maxAcceleration, float timeToTarget) {
		this->character = character;
		this->target = target;
		this->maxAcceleration = maxAcceleration;
		this->timeToTarget = timeToTarget;
	}

SteeringOutput DynamicVelocityMatching::getSteering() {
	SteeringOutput steering = SteeringOutput();

	steering.Linear = target->kinematic.Velocity - character->kinematic.Velocity;
	steering.Linear /= timeToTarget;

	if (glm::length(steering.Linear)>maxAcceleration) {
		steering.Linear = glm::normalize(steering.Linear);
		steering.Linear *= maxAcceleration;
	}

	steering.Angular = 0;
	return steering;
}


Align::Align(GameObject* character, GameObject* target, float maxAngular, float maxRotation, float targetRadius, float slowRadius, float timeToTarget) {
		this->character = character;
		this->target = target;
		this->maxAngular = maxAngular;
		this->maxRotation = maxRotation;
		this->targetRadius = targetRadius;
		this->slowRadius = slowRadius;
		this->timeToTarget = timeToTarget;
	}

SteeringOutput Align::getSteering() {

	SteeringOutput steering = SteeringOutput();
	float rotation, rotationSize, targetRotation, angular;
	float targetRadians = glm::radians(targetRadius);
	float slowRadians = glm::radians(slowRadius);
	rotation = this->target->kinematic.Orientation - this->character->kinematic.Orientation;
	rotation = mapToRange(rotation);
	rotationSize = glm::abs(rotation);
	if (rotationSize < targetRadians) {
		this->character->kinematic.Rotation = 0.0f;
		return steering;
	}

	if (rotationSize > slowRadians)
		targetRotation = maxRotation;
	else targetRotation = maxRotation * rotationSize / slowRadians;

	targetRotation *= rotation / rotationSize;

	steering.Angular = targetRotation - this->character->kinematic.Rotation;
	steering.Angular /= timeToTarget;

	angular = glm::abs(steering.Angular);
	if (angular > maxAngular) {
		steering.Angular /= angular;
		steering.Angular *= maxAngular;
	}
	
	steering.Linear = glm::vec3( 0.0f, 0.0f, 0.0f);
	return steering;
}

Separation::Separation(GameObject* character, std::list<GameObject*> &collisionables, float maxAcceleration, float threshold, float decayCoefficient) :  maxAcceleration(maxAcceleration), threshold(threshold), decayCoefficient(decayCoefficient) {
	this->character = character;
	this->collisionables = collisionables;
}

SteeringOutput Separation::getSteering() {
	SteeringOutput steering = SteeringOutput();
	float distance;
	for (auto collisionable : collisionables) {
		if (collisionable != this->character) {
			distance = glm::length(collisionable->kinematic.Position - this->character->kinematic.Position);
			if (distance < threshold) {
				steering.Linear += -std::min<float>(decayCoefficient / (distance * distance), maxAcceleration) *glm::normalize(collisionable->kinematic.Position - this->character->kinematic.Position);
			}
		}
	}
	return steering;
}
