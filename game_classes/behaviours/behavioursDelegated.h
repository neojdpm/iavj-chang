#ifndef BEHAVIOURS_D_H
#define BEHAVIOURS_D_H
#include "behaviours.h"
class Face : public Align {
public:
	Face(GameObject* character, GameObject* target, float maxAngular, float maxRotation, float targetRadius, float slowRadius, float timeToTarget = 0.1f)
		: Align(character, target, maxAngular, maxRotation, targetRadius, slowRadius, timeToTarget = 0.1f) {
	}

	SteeringOutput getSteering() {
		SteeringOutput steering = SteeringOutput();
		glm::vec3 direction;
		direction = this->target->kinematic.Position - this->character->kinematic.Position;

		if (glm::length(direction) == 0)
			return steering;

		//Guardar valores anteriores
		float orientation = this->target->kinematic.Orientation;
		this->target->kinematic.Orientation = glm::atan(direction.x, direction.z);
		//Calcular steering
		steering = Align::getSteering();

		//Regresar valores anteriores
		this->target->kinematic.Orientation = orientation;
		return steering;
	}
};

class LookWhereYouAreGoing : public Align {
public:
	LookWhereYouAreGoing(GameObject* character, float maxAngular, float maxRotation, float targetRadius, float slowRadius, float timeToTarget = 0.1f)
		: Align(character, new GameObject(), maxAngular, maxRotation, targetRadius, slowRadius, timeToTarget = 0.1f) {
	}

	SteeringOutput getSteering() {
		SteeringOutput steering = SteeringOutput();
		if (glm::length(character->kinematic.Velocity) < glm::epsilon<float>()) {
			character->kinematic.Rotation = 0.0f;
			return steering;
		}
		this->target->kinematic.Orientation = glm::atan(character->kinematic.Velocity.x, character->kinematic.Velocity.z);

		return Align::getSteering();
	}
};

class DynamicPursue : public DynamicSeek {
public:
	float maxPrediction;

	DynamicPursue(GameObject* character, GameObject* target, float maxAcceleration, float maxPrediction)
		: DynamicSeek(character, target, maxAcceleration) {
		this->maxPrediction = maxPrediction;
	}

	SteeringOutput getSteering() {
		float distance, speed, prediction;

		glm::vec3 direction = this->target->kinematic.Position - this->character->kinematic.Position;
		distance = glm::length(direction);
		speed = glm::length(this->character->kinematic.Velocity);

		if (speed <= distance / maxPrediction) prediction = maxPrediction;
		else prediction = distance / speed;


		target->kinematic.Position += target->kinematic.Velocity * prediction;
		SteeringOutput steering = DynamicSeek::getSteering();
		target->kinematic.Position -= target->kinematic.Velocity * prediction;
		return steering;
	}
};

class DynamicEvade : public DynamicFlee {
public:
	float maxPrediction;

	DynamicEvade(GameObject* character, GameObject* target, float maxAcceleration, float maxPrediction)
		: DynamicFlee(character, target, maxAcceleration) {
		this->maxPrediction = maxPrediction;
	}

	SteeringOutput getSteering() {
		float distance, speed, prediction;

		glm::vec3 direction = -this->target->kinematic.Position + this->character->kinematic.Position;
		distance = glm::length(direction);
		speed = glm::length(this->character->kinematic.Velocity);

		if (speed <= distance / maxPrediction) prediction = maxPrediction;
		else prediction = distance / speed;


		target->kinematic.Position += target->kinematic.Velocity * prediction;
		SteeringOutput steering = DynamicFlee::getSteering();
		target->kinematic.Position -= target->kinematic.Velocity * prediction;
		return steering;
	}
};

class Wander : public DynamicSeek {
public:
	float wanderOffset, wanderRadius, wanderRate, wanderOrientation, maxAcceleration, tickRate = 0.25f;
	Wander(GameObject* character, float wanderOffset, float wanderRadius, float wanderRate, float wanderOrientation, float maxAcceleration) :
		DynamicSeek(character, new GameObject(), maxAcceleration) {
		this->wanderOffset = wanderOffset;
		this->wanderRadius = wanderRadius;
		this->wanderRate = wanderRate;
		this->wanderOrientation = wanderOrientation;
		this->maxAcceleration = maxAcceleration;
		this->target->setMesh(ResourceManager::GetMesh("box"));
	}

	SteeringOutput getSteering() {
		float targetOrientation;
		if (lastTick + tickRate < glfwGetTime()) {
			lastTick = glfwGetTime();
			this->wanderOrientation += randomBinomial() * wanderRate;
			targetOrientation = this->wanderOrientation + this->character->kinematic.Orientation;
			this->target->kinematic.Position = this->character->kinematic.Position + this->wanderOffset * asVector(this->character->kinematic.Orientation);
			this->target->kinematic.Position += this->wanderRadius * asVector(targetOrientation);
		}

		if (Globals::debugMode) this->target->Draw(glm::vec3(0.0f, 1.0f, 0.0f));

		return DynamicSeek::getSteering();
	}
private:
	float lastTick = 0.0f;
};


class PathFollowing : public DynamicArrive {
public:
	Spline* path;
	float currentParam;
	float positionOffset;
	float pathOffset;

	PathFollowing(GameObject* character, Spline* path, float positionOffset, float pathOffset, float maxAcceleration, float maxSpeed, float targetRadius, float slowRadius, float timeToTarget = 0.1)
		: DynamicArrive(character, new GameObject(), maxAcceleration, maxSpeed, targetRadius, slowRadius, timeToTarget) {
		this->path = path;
		this->pathOffset = pathOffset;
		this->positionOffset = positionOffset;
		this->target->setMesh(ResourceManager::GetMesh("box"));
		this->target->kinematic.Position = character->kinematic.Position;
		this->target->Size = glm::vec3(0.5f);
		UpdatePath();
	}

	void UpdatePath() {
		if (path->size > 0) {
			currentParam = path->getParam(target->kinematic.Position);
			target->kinematic.Position = path->getPoint(currentParam);
		}
	}

	void setPosition(glm::vec3 position) {
		target->kinematic.Position = position;
	}

	SteeringOutput getSteering() {
		float distance = glm::length(character->kinematic.Position - target->kinematic.Position);
		if (currentParam < 0.99 && distance < positionOffset) {
			currentParam += pathOffset / path->size;
			target->kinematic.Position = path->getPoint(currentParam);
		}

		if (Globals::debugMode) {
			//this->target->Draw(glm::vec3(0.0f, 1.0f, 0.0f));
			path->Draw(glm::vec3(1.0f, 1.0f, 1.0f));
		}

		return DynamicArrive::getSteering();
	}
};

class ObstacleAvoidance : public DynamicSeek {
public:
	float avoidDistance;
	float lookAhead;
	std::list<GameObject*> collisionables;

	ObstacleAvoidance(GameObject* character, std::list<GameObject*> &collisionables, float avoidDistance, float lookAhead, float maxAcceleration)
		: DynamicSeek(character, new GameObject(), maxAcceleration) {
		this->avoidDistance = avoidDistance;
		this->lookAhead = lookAhead;
		this->collisionables = collisionables;
	}

	SteeringOutput getSteering() {
		SteeringOutput steering = SteeringOutput();

		glm::vec3 velocity = glm::normalize(character->kinematic.Velocity);
		float orientation = atan2(velocity.x, velocity.z);
		Ray ray1 = Ray(character->kinematic.Position, orientation);
		Ray ray2 = Ray(character->kinematic.Position, orientation + sin(15)*0.7f); // 
		Ray ray3 = Ray(character->kinematic.Position, orientation - sin(15)*0.7f);

		std::vector<Collision> normals;
		normals.push_back(getCollision(ray1, ray2, ray3, lookAhead, collisionables));
		//normals.push_back(getCollision(ray2, lookAhead*0.66, collisionables));
		//normals.push_back(getCollision(ray3, lookAhead*0.66, collisionables));
		if (Globals::debugMode) {
			ray1.DrawRay(lookAhead);
			ray2.DrawRay(lookAhead*0.66, glm::vec3(1.0f, 1.0f, 0.0f));
			ray3.DrawRay(lookAhead*0.66, glm::vec3(1.0f, 1.0f, 0.0f));
		}

		for (auto collision : normals)
		{
			if (collision.normal != glm::vec3()) {
				if (Globals::debugMode) Ray(collision.point, glm::reflect(ray1.direction, collision.normal)).DrawRay(avoidDistance, glm::vec3(0.0f, 0.0f, 1.0f));
				target->kinematic.Position = collision.point + collision.normal * avoidDistance;										  
				target->kinematic.Position.y = character->kinematic.Position.y;
				return DynamicSeek::getSteering();
			}
		}
		return steering;
	}
};

class SeekPathingRandomized : public PathFollowing {
public:
	Graph* graph;

	SeekPathingRandomized(GameObject* character, Graph* graph, float positionOffset, float pathOffset, float maxAcceleration, float maxSpeed, float targetRadius, float slowRadius, float timeToTarget = 0.1)
		:PathFollowing(character, new Spline(), positionOffset, pathOffset, maxAcceleration, maxSpeed, targetRadius, slowRadius, timeToTarget) {
		this->graph = graph;
		recalculatePath();
		this->path->setShader(ResourceManager::GetShader("lines"));
	}

	void continuePath() {
		if (!connections.empty()) {
			this->path->addPoint3(connections.front()->toNode->center());
			connections.pop_front();
			this->path->updateRenderData();
		}
	}
	void removePath() {
		this->path->popPoint3();
	}

	void recalculatePath() {
		int RandIndex = rand() % graph->nodes.size() - 1;
		previousNodeObj = graph->nodes[RandIndex];
		Node * nodeChar = graph->getNode(character->kinematic.Position);
		if (nodeChar && previousNodeObj) {
			 this->path->clear();
			 connections = pathFindAStar(nodeChar, previousNodeObj);
			 this->path->addPoint3(character->kinematic.Position);
			 continuePath();
			 continuePath();
			 continuePath();
			 continuePath();
			 UpdatePath();
		}
		
	}

	SteeringOutput getSteering() {
		if (Globals::debugMode) for (auto con : connections) Debug::DrawLine(con->fromNode->center(), con->toNode->center(), glm::vec3(1.0f, 0.5f, 0.0f)); // Draw path
		
		/* Make the objetive position the last path point.*/
		if (this->path->size <= 1 || glm::length(character->kinematic.Position - this->path->points.front()) > 4.0f) {
			recalculatePath();
		}
		if (currentParam > 0.4f && this->path->size > 1) {
			removePath();
			continuePath();
			UpdatePath();
		}
		return PathFollowing::getSteering();
	}

private:
	std::list<Connection*> connections;
	Node* previousNodeObj;
};

class SeekPathing : public PathFollowing {
public:
	GameObject* objetive;
	Graph* graph;

	SeekPathing(GameObject* character, GameObject* objetive, Graph* graph, float positionOffset, float pathOffset, float maxAcceleration, float maxSpeed, float targetRadius, float slowRadius, float timeToTarget = 0.1)
		:PathFollowing(character, new Spline(), positionOffset, pathOffset, maxAcceleration, maxSpeed, targetRadius, slowRadius, timeToTarget) {
		this->graph = graph;
		this->objetive = objetive;
		recalculatePath();
		this->path->setShader(ResourceManager::GetShader("lines"));
	}

	void continuePath() {
		if (!connections.empty()) {
			if (connections.size() == 1 && previousNodeObj) this->path->addPoint3(objetive->kinematic.Position);
			else this->path->addPoint3(connections.front()->toNode->center());
			connections.front()->cost /= 2.5f; // Resetting the path cost
			connections.pop_front();
			this->path->updateRenderData();
		}
	}
	void removePath() {
		this->path->popPoint3();
	}

	void recalculatePath() {
		previousNodeObj = graph->getNode(objetive->kinematic.Position);
		Node * nodeChar = graph->getNode(character->kinematic.Position);
		if (nodeChar && previousNodeObj) {
			this->path->clear();
			connections = pathFindAStar(nodeChar, previousNodeObj);
			for (Connection* conn : connections) {
				conn->cost *= 2.5f; // Augmenting the path cost
			}

			this->path->addPoint3(character->kinematic.Position);
			continuePath();
			continuePath();
			continuePath();
			continuePath();
			UpdatePath();
		}

	}

	SteeringOutput getSteering() {
		/* if the target isn't in the same triangle or is in no triangle, recalculate*/
		if (!previousNodeObj || !checkNodePosition(objetive->kinematic.Position, previousNodeObj)) {
			for (Connection* conn : connections) {
				conn->cost /= 2.5f; // Resetting the path cost
			}
			recalculatePath();
		}
		if (Globals::debugMode) for (auto con : connections) Debug::DrawLine(con->fromNode->center(), con->toNode->center(), glm::vec3(1.0f, 0.5f, 0.0f)); // Draw path
		
		/* Make the objetive position the last path point.*/
		if (connections.empty() && previousNodeObj) {
			this->path->popPoint3back();
			this->path->addPoint3(objetive->kinematic.Position);
			if (this->path->size == 1) setPosition(objetive->kinematic.Position);
			this->path->updateRenderData();
		}
		if (currentParam > 0.4f && this->path->size > 1) {
			removePath();
			continuePath();
			UpdatePath();
		}
		return PathFollowing::getSteering();
	}

private:
	std::list<Connection*> connections;
	Node* previousNodeObj;
};
#endif