#ifndef BEHAVIOURS_H
#define BEHAVIOURS_H

#include "../ray.h"
#include "../../headers/game_object.h"
#include "../splines.h"
#include <GLFW/glfw3.h>

class DynamicSeek : public Behaviour {
public:
	GameObject* target;
	float maxAcceleration;

	DynamicSeek(GameObject* character, GameObject* target, float maxAcceleration);
	SteeringOutput getSteering();
};

class DynamicFlee : public Behaviour {
public:
	GameObject* target;
	float maxAcceleration;

	DynamicFlee(GameObject* character, GameObject* target, float maxAcceleration);
	SteeringOutput getSteering();
};

class DynamicArrive : public Behaviour {
public:
	GameObject* target;
	float maxAcceleration;
	float maxSpeed;
	float targetRadius;
	float slowRadius;
	float timeToTarget = 0.1;

	DynamicArrive(GameObject* character, GameObject* target, float maxAcceleration, float maxSpeed, float targetRadius, float slowRadius, float timeToTarget = 0.1);
	SteeringOutput getSteering();

};

class DynamicVelocityMatching : public Behaviour {
public:
	GameObject* target;
	float maxAcceleration;
	float timeToTarget = 0.1f;

	DynamicVelocityMatching(GameObject* character, GameObject* target, float maxAcceleration, float timeToTarget = 0.1f);
	SteeringOutput getSteering();
};

class Align : public Behaviour {
public:
	GameObject* target;
	float maxAngular;
	float maxRotation;
	float targetRadius;
	float slowRadius;
	float timeToTarget = 0.1f;

	Align(GameObject* character, GameObject* target, float maxAngular, float maxRotation, float targetRadius, float slowRadius, float timeToTarget = 0.1f);
	SteeringOutput getSteering();
};

class Separation : public Behaviour {
public:
	float maxAcceleration;
	std::list<GameObject*> collisionables;
	float threshold, decayCoefficient;
	Separation(GameObject* character, std::list<GameObject*> &collisionables, float maxAcceleration, float threshold, float decayCoefficient);
	SteeringOutput getSteering();
};



#endif