#ifndef UTILITIES_H
#define UTILITIES_H
#include <glm/gtc/type_ptr.hpp>
#include <glad/glad.h>

// maps an orientation to [-pi, pi]
static double mapToRange(double orientation)
{
	while ((orientation > glm::pi<double>()) || (orientation < -glm::pi<double>()))
	{
		if (orientation > glm::pi<double>())
			orientation -= glm::two_pi<double>();
		else if (orientation < -glm::pi<double>())
			orientation += glm::two_pi<double>();
	}
	return orientation;
}

// Returns an 2d angle to its respective vector in the 3d space
static glm::vec3 asVector(float angle) {
	return glm::vec3(glm::sin(angle), 0.0f, glm::cos(angle));
}

// Random function from -1 to 1 
static float randomBinomial() {
	return (float) rand() / (RAND_MAX) - (float) rand() / (RAND_MAX);
}

// Get the combinatory number of (n k)
static int binomialCoeff(int n, int k)
{
	int result = 1;
	if (k > n - k) k = n - k;

	for (int i = 0; i < k; ++i)
	{
		result *= (n - i);
		result /= (i + 1);
	}

	return result;
}


#endif