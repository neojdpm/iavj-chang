#ifndef RAY_H
#define RAY_H
#include "../headers/resource_manager.h"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/intersect.hpp>
#include <glm/gtx/projection.hpp>
#include "Debug\debug.h"
#include "Graph\graph.h"
#include "../headers/game_object.h"

class Ray {
public:
	glm::vec3 origin;
	glm::vec3 direction;

	Ray() {}

	Ray(glm::vec3 origin, float orientation) : origin(origin) {
		direction = asVector(orientation);
	}

	Ray(glm::vec3 origin, glm::vec3 orientation) : origin(origin), direction(orientation){
	}

	glm::vec3 GetPoint(float distance) {
		return origin + direction * distance;
	}

	glm::vec3 getNormalPlane(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 cut) {
		glm::vec3 normal = glm::normalize(glm::cross(p2 - p1, p3 - p2));
		if (glm::length(origin - (cut + normal)) < glm::length(origin - (cut - normal))) return normal;
		return -normal;
	}

	void DrawRay(float distance, glm::vec3 color = glm::vec3(1.0f)) {
		glm::vec3 endRay = GetPoint(distance);
		Debug::DrawLine(origin, endRay, color);
	}

};
struct Collision {
	glm::vec3 normal = glm::vec3();
	glm::vec3 point = glm::vec3();
};

static Collision getCollision(Ray ray, Ray ray2, Ray ray3, float radius, std::list<GameObject*> collisionables)
{

	Collision collision   = Collision();
	glm::vec3 normal      = glm::vec3();
	glm::vec3 innerNormal = glm::vec3();
	Ray intersected;

	for (GameObject* object : collisionables) {
		if (object->is_model) {
			for (auto mesh : object->model->meshes) {
				string str = mesh.name.data;
				if (str == Globals::walls_name) {
					std::vector<unsigned int> indices = mesh.indices;
					std::vector<Vertex> vertices = mesh.vertices;
					float closest = (float)INT_MAX;
					for (size_t i = 0; i < indices.size(); i += 3) {
						float lookahead = 0;
						Vertex vertex = vertices[indices[i]];
						Vertex vertex2 = vertices[indices[i + 1]];
						Vertex vertex3 = vertices[indices[i + 2]];
						// Getting all triangles of the mesh
						glm::vec3 p1 = (vertex.Position + object->kinematic.Position)* object->Size;
						glm::vec3 p2 = (vertex2.Position + object->kinematic.Position)* object->Size;
						glm::vec3 p3 = (vertex3.Position + object->kinematic.Position)* object->Size;
						glm::vec3 cut;
						if (glm::intersectRayTriangle(ray.origin, ray.direction, p1, p2, p3, cut)) {
							intersected = ray;
							lookahead = radius;
							float z = 1.0 - cut.x - cut.y;
							cut = p1 * z + p2 * cut.x + p3 * cut.y;
							float distance = glm::length(intersected.origin - cut);
							if (distance < closest && distance < lookahead)
							{/*
								Debug::DrawLine(p1, p2, glm::vec3(0.0f, 1.0f, 0.0f));
								Debug::DrawLine(p2, p3, glm::vec3(0.0f, 1.0f, 0.0f));
								Debug::DrawLine(p3, p1, glm::vec3(0.0f, 1.0f, 0.0f));*/
								closest = distance;
								innerNormal = vertex.Normal;
								//innerNormal = intersected.getNormalPlane(p1, p2, p3, cut);
								collision.point = cut;
							}
						}
						if (glm::intersectRayTriangle(ray3.origin, ray3.direction, p1, p2, p3, cut)) {
							intersected = ray3;
							lookahead = radius*0.66;
							float z = 1.0 - cut.x - cut.y;
							cut = p1 * z + p2 * cut.x + p3 * cut.y;
							float distance = glm::length(intersected.origin - cut);
							if (distance < closest && distance < lookahead)
							{/*
								Debug::DrawLine(p1, p2, glm::vec3(0.0f, 1.0f, 0.0f));
								Debug::DrawLine(p2, p3, glm::vec3(0.0f, 1.0f, 0.0f));
								Debug::DrawLine(p3, p1, glm::vec3(0.0f, 1.0f, 0.0f));*/
								closest = distance;
								innerNormal = vertex.Normal;
								collision.point = cut;
							}
						}
						if (glm::intersectRayTriangle(ray2.origin, ray2.direction, p1, p2, p3, cut)) {
							intersected = ray2;
							lookahead = radius*0.66;
							float z = 1.0 - cut.x - cut.y;
							cut = p1 * z + p2 * cut.x + p3 * cut.y;
							float distance = glm::length(intersected.origin - cut);
							if (distance < closest && distance < lookahead)
							{/*
								Debug::DrawLine(p1, p2, glm::vec3(0.0f, 1.0f, 0.0f));
								Debug::DrawLine(p2, p3, glm::vec3(0.0f, 1.0f, 0.0f));
								Debug::DrawLine(p3, p1, glm::vec3(0.0f, 1.0f, 0.0f));*/
								closest = distance;
								innerNormal = vertex.Normal;
								collision.point = cut;
							}
						}
					}
				}
				collision.normal += innerNormal;
			}
		}
		else {
			std::cout << "Try with models" << std::endl;
		}
	}

	return collision;
}

/* TODO: COLLISION RESOLUTION MODULE*/

static Collision getCollisionFloor(Ray ray, Node* node, float radius) {
	Collision collision = Collision();
	Ray intersected;
	glm::vec3 p1 = node->p1;
	glm::vec3 p2 = node->p2;
	glm::vec3 p3 = node->p3;
	glm::vec3 cut;
	if (glm::intersectRayTriangle(ray.origin, ray.direction, p1, p2, p3, cut)) {
		intersected = ray;
		float lookahead = radius;
		float z = 1.0 - cut.x - cut.y;
		cut = p1 * z + p2 * cut.x + p3 * cut.y;
		float distance = glm::length(intersected.origin - cut);
		if (distance < lookahead)
		{
			collision.normal = node->normal;
			collision.point = cut;
		}
	}
	return collision;
}

// expensive thing ray cast from mesh triangle centers
static void calculateCollisionWall(GameObject* player, float radius, std::list<GameObject*> collisionables)
{

	Collision collision = Collision();
	glm::vec3 normal = glm::vec3();
	glm::vec3 innerNormal = glm::vec3();
	Ray intersected;

	Ray ray = Ray(player->kinematic.Position, player->kinematic.Velocity);

	for (GameObject* object : collisionables) {
		if (object->is_model) {
			for (auto mesh : object->model->meshes) {
				string str = mesh.name.data;
				if (str == Globals::walls_name) {
					std::vector<unsigned int> indices = mesh.indices;
					std::vector<Vertex> vertices = mesh.vertices;
					float closest = (float)INT_MAX;
					for (size_t i = 0; i < indices.size(); i += 3) {
						float lookahead = 0;
						Vertex vertex = vertices[indices[i]];
						Vertex vertex2 = vertices[indices[i + 1]];
						Vertex vertex3 = vertices[indices[i + 2]];
						// Getting all triangles of the mesh
						glm::vec3 p1 = (vertex.Position + object->kinematic.Position)* object->Size;
						glm::vec3 p2 = (vertex2.Position + object->kinematic.Position)* object->Size;
						glm::vec3 p3 = (vertex3.Position + object->kinematic.Position)* object->Size;
						glm::vec3 cut;
						if (glm::intersectRayTriangle(ray.origin, ray.direction, p1, p2, p3, cut)) {
							intersected = ray;
							lookahead = radius;
							float z = 1.0 - cut.x - cut.y;
							cut = p1 * z + p2 * cut.x + p3 * cut.y;
							float distance = glm::length(intersected.origin - cut);
							if (distance < closest && distance < lookahead)
							{
								if (Globals::debugMode) {
									Debug::DrawLine(p1, p2, glm::vec3(0.0f, 1.0f, 0.0f));
									Debug::DrawLine(p2, p3, glm::vec3(0.0f, 1.0f, 0.0f));
									Debug::DrawLine(p3, p1, glm::vec3(0.0f, 1.0f, 0.0f));
								}
								std::cout << "COLLISION" << std::endl;
								closest = distance;
								innerNormal = vertex.Normal;
								//innerNormal = intersected.getNormalPlane(p1, p2, p3, cut);
								collision.point = cut;
							}
						}
					}
				}
				collision.normal += innerNormal;
			}
		}
		else {
			std::cout << "Try with models" << std::endl;
		}
	}
	if (collision.normal != glm::vec3()) {
		glm::vec3 x = glm::vec3(player->kinematic.Velocity.x, 0.0f, 0.0f);
		glm::vec3 z = glm::vec3(0.0f, 0.0f, player->kinematic.Velocity.z);
		std::cout << to_string(glm::proj(x, collision.normal)) << std::endl;
		std::cout << glm::dot(x, collision.normal) << std::endl;;
		std::cout << glm::dot(z, collision.normal) << std::endl;;
	}
}

static void calculateCollisionFloor(Graph* graph, GameObject* object, float radius)
{
	Node* node = graph->getNode(object->kinematic.Position, object->node);
	if (node) {
		Ray ray = Ray(object->kinematic.Position, Globals::gravity);
		if (getCollisionFloor(ray, node, radius).normal != glm::vec3()) {
			object->jumping = false;
			object->kinematic.Velocity.y = 0.0f;
		}
	}
}

#endif