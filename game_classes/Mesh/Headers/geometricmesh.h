#ifndef G_MESH_H
#define G_MESH_H

#include "mesh.h"

class CubeMesh : public Mesh {
public:
	CubeMesh(Shader &shader, int drawType=GL_TRIANGLES, int drawSize=36);
	void initRenderData();
};

class QuadMesh : public Mesh {
public:
	QuadMesh(Shader &shader, int drawType = GL_TRIANGLES, int drawSize = 6);
	void initRenderData();
};
#endif