#ifndef MESH_H
#define MESH_H

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "../../utilities.h"
#include "../../../headers/shader_c.h"
#include "../../../headers/globals.h"
#include "../../../headers/textures.h"
#include "../../Debug/debug.h"
#include <vector>
#include <string>



struct Material {
	glm::vec3 diffuse;
	glm::vec3 specular;
	float opacity;
};

struct Vertex {
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
	glm::vec3 Tangent;
	glm::vec3 Bitangent;
};

struct Textures {
	unsigned int id;
	std::string type;
	aiString path;
};

class Mesh {
public:
	aiString name;
	Shader shader;
	Texture texture;
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<Textures> textures;
	std::vector<Material> materials;
	double time;
	int drawType;
	int drawSize;
	bool with_texture = false;

	GLuint quadVAO, VBO, EBO;

	Mesh() { }
	Mesh(aiString name, std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Textures> textures, std::vector<Material> materials) {
		this->vertices = vertices;
		this->name = name;
		this->indices = indices;
		this->textures = textures;
		this->materials = materials;

		setupMesh();
	}
	Mesh(Shader &shader, int drawType, int drawSize) : drawType(drawType), drawSize(drawSize) {
		this->shader = shader;
		this->initRenderData();
	}

	// Initializes and configures the quad's buffer and vertex attributes
	virtual void initRenderData() {
	}

	void setShader(Shader &shader) {
		this->shader = shader;
	}

	void setTexture(Texture &texture) {
		this->texture = texture;
		with_texture = true;
	}

	void Draw(Shader* shader, glm::vec3 size, glm::vec3 position, float orientation) {
		// bind appropriate textures
		unsigned int diffuseNr = 1;
		unsigned int specularNr = 1;
		unsigned int normalNr = 1;
		unsigned int heightNr = 1;
		bool has_alpha = false;

		glm::mat4 model;
		model = glm::translate(model, position);
		model = glm::rotate(model, orientation , glm::vec3(0.0f, 1.0f, 0.0f));
		model = glm::scale(model, size); // Last scale

		shader->Use();
		std::string str = "Ghostly";
		if (name.data == str) shader->setBool("is_ghost", true);
		// Model properties
		shader->setFloat("material.shininess", 32.0f);
		shader->setBool("is_texture", false);
		for (unsigned int i = 0; i < textures.size(); i++)
		{
			shader->setBool("is_texture", true);
			glActiveTexture(GL_TEXTURE0 + i); // active proper texture unit before binding
											  // retrieve texture number (the N in diffuse_textureN)
			std::stringstream ss;
			std::string number;
			std::string name = textures[i].type;
			if (name == "texture_diffuse")
				ss << diffuseNr++; // transfer unsigned int to stream
			else if (name == "texture_specular")
				ss << specularNr++; // transfer unsigned int to stream
			else if (name == "texture_normal")
				ss << normalNr++; // transfer unsigned int to stream
			else if (name == "texture_height")
				ss << heightNr++; // transfer unsigned int to stream
			number = ss.str();
			// now set the sampler to the correct texture unit
			glUniform1i(glGetUniformLocation(shader->ID, (name + number).c_str()), i);
			// and finally bind the texture
			glBindTexture(GL_TEXTURE_2D, textures[i].id);
		}
		
		for (unsigned int i = 0; i < materials.size(); i++)
		{
			shader->setVec3("material.diffuse", materials[i].diffuse);
			shader->setVec3("material.specular", materials[i].specular);
			shader->setFloat("material.opacity", materials[i].opacity);
			if (materials[i].opacity < 1) has_alpha = true;
		}
		shader->setMat4("model", model);
		// draw mesh
		glBindVertexArray(quadVAO);
		//if (has_alpha) glDisable(GL_DEPTH_TEST);
		glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
		//if (has_alpha) glEnable(GL_DEPTH_TEST);
		glBindVertexArray(0);

		// always good practice to set everything back to defaults once configured.
		glActiveTexture(0);

		shader->setBool("is_ghost", false);
	}

	void setTime(double dt) {
		this->time = dt;
	}

	void DrawMesh( glm::vec3 size, glm::vec3 position, float orientation, glm::vec3 color)
	{
		Shader *shader = &this->shader;
		shader->Use();
		int frame = (int)(this->time * 10) % 31;
		shader->setInt("frame", frame);
		glm::mat4 model;
		model = glm::translate(model, position);
		model = glm::rotate(model, 1.68f, glm::vec3(1.0f, 0.0f, 0.0f));
		model = glm::scale(model, size); // Last scale
		this->shader.setVec4("oColor", glm::vec4(0.5f, 1.0f, 1.0f, 1.0f));
		if (with_texture) {
			glActiveTexture(GL_TEXTURE0);
			texture.Bind();
		}
		this->shader.setMat4("model", model);

		glBindVertexArray(this->quadVAO);
		glDrawArrays(drawType, 0, drawSize);
		glBindVertexArray(0);
	}

	void setupMesh()
	{
		glGenVertexArrays(1, &quadVAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &EBO);

		glBindVertexArray(quadVAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);

		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int),
			&indices[0], GL_STATIC_DRAW);

		// vertex positions
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
		// vertex normals
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));
		// vertex texture coords
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));
		// vertex tangent
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Tangent));
		// vertex bitangent
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Bitangent));

		glBindVertexArray(0);
	}
};
#endif

