#ifndef SPLINES_H
#define SPLINES_H

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "utilities.h"
#include <list>
#include <vector>

class Spline
{
public:
	// Splines size
	GLuint  size;
	// Splines Points
	std::list<glm::vec3> points = std::list<glm::vec3>();	

	Spline() {
		size = 0;
	}

	Spline(GLuint size) : size(size) {
	}

	void clear() {
		points = std::list<glm::vec3>();
		size = 0;
	}

	void addPoint3(glm::vec3 point) {
		points.push_back(glm::vec3(point.x, 0.0f, point.z)); // 0.0f on Y 2d 1/2
		size++;
	}

	void addPoint3(float x, float y, float z) {
		points.push_back(glm::vec3(x, 0.0f, z)); // 0.0f on Y 2d 1/2
		size++;
	}

	void popPoint3() {
		points.pop_front();
		size--;
	}

	void popPoint3back() {
		if (!points.empty()) {
			points.pop_back();
			size--;
		}
	}
	glm::vec3 getPoint(GLfloat t) {
		glm::vec3 point;
		int i = 0, size = this->size - 1;
		for (glm::vec3 p : points) {
			point += p * (float)(binomialCoeff(size, i) * pow(t, i) * pow(1 - t, size - i));
			i++;
		}
		return point;
	}

	float getParam(glm::vec3 position, int iterations = 20) {
		glm::vec3 point;
		float new_param = 0.5, param = 0.5, length, lower_length, upper_length; // Params starting from half the spline

		// Initial length value
		length = glm::length(getPoint(param) - position);

		for (int i = 0; i <= iterations; i++) {
			new_param = new_param / 2; // 0 < param < 1

			lower_length = glm::length(getPoint(param - new_param) - position); //lower bound comparation
			upper_length = glm::length(getPoint(param + new_param) - position); //upper bound comparation
			
			if (lower_length <= upper_length) {
				param -= new_param;
				length = lower_length;
			}
			else {
				param += new_param;
				length = upper_length;
			}
		}

		return param;
	}

	void setShader(Shader &shader) {
		this->shader = shader;

		glGenVertexArrays(2, this->quadVAO);
		glGenBuffers(2, VBO);

		/* USING A MAX SIZE OF 5 AND 10 LINES PER POINT*/
		short int maxSize = 5, maxQuantity = 50;
		// Curve
		glBindVertexArray(this->quadVAO[0]);
		glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
		glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(GLfloat) * maxQuantity, NULL, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);
		// Points
		glBindVertexArray(this->quadVAO[1]);
		glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
		glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(GLfloat) * maxSize, NULL, GL_DYNAMIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);

		this->updateRenderData();
	}

	void updateRenderData(int lines = 10) {
		// Configure VAO/VBO
		this->lines = lines;
		int quantity = size * lines;
		float deltat = 1.0f / quantity;
		float delta = 0;
		std::vector<float> vertices(quantity * 3);
		std::vector<float> points(size * 3);

		int i = 0;
		// Populating the curve
		for (i = 0; i < quantity * 3; i += 3) {
			glm::vec3 point = getPoint(delta);
			vertices[i] = point.x;
			vertices[i + 1] = point.y;
			vertices[i + 2] = point.z;
			delta += deltat;
		}
		i = 0;
		// Populating the points
		for (auto p : this->points) {
			points[i] = p.x;
			points[i + 1] = p.y;
			points[i + 2] = p.z;
			i += 3;
		}
		// Curve
		glBindVertexArray(this->quadVAO[0]);
		glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
		glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof(GLfloat) , vertices.data());
		glEnableVertexAttribArray(0);
		// Points
		glBindVertexArray(this->quadVAO[1]);
		glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
		glBufferSubData(GL_ARRAY_BUFFER, 0, points.size() * sizeof(GLfloat), points.data());
		glEnableVertexAttribArray(0);

		// position attribute
		glBindVertexArray(0);
	}

	void Draw(glm::vec3 color)
	{
		// Prepare transformations
		this->shader.Use();
		glm::mat4 model;

		this->shader.setVec4("ourColor", glm::vec4(color, 1.0f));
		this->shader.setMat4("model", model);

		glBindVertexArray(this->quadVAO[0]);
		glDrawArrays(GL_LINE_STRIP_ADJACENCY, 0, size*lines);
		glBindVertexArray(this->quadVAO[1]);
		glEnable(GL_PROGRAM_POINT_SIZE_EXT);
		glPointSize(5);
		glDrawArrays(GL_POINTS, 0, size);
		glBindVertexArray(0);
	}

private:
	Shader shader;
	int lines;
	GLuint quadVAO[2], VBO[2];
};

#endif