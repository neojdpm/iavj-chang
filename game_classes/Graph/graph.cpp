#include "graph.h"

struct NodeRecord {
	Node* node;
	Connection* connection;
	float costSoFar;
	float estimatedTotalCost;
	NodeRecord* parent;

	/*
	bool operator<(NodeRecord* t) {
		return estimatedTotalCost < t->estimatedTotalCost;
	}*/
	inline bool operator==(NodeRecord a) {
		if (a.node == node && a.connection == connection && a.costSoFar == costSoFar 
			&& a.estimatedTotalCost == estimatedTotalCost && a.parent == parent)
			return true;
		else
			return false;
	}
};

struct cmp_ptr
{
	bool operator()(const NodeRecord lhs, const NodeRecord rhs) const
	{
		return lhs.estimatedTotalCost < rhs.estimatedTotalCost;
	}
};

typedef std::set<NodeRecord, cmp_ptr> nset; // priority queue
typedef std::unordered_map<Node*, NodeRecord> node_map; // closed list and  priority queue item checker

float euclidean_heuristic(Node* s, Node* g) {
	return glm::length(s->center() - g->center());
}


std::list<Connection*> pathFindAStar(Node* start, Node* goal) {

	std::list<Connection*> path = std::list<Connection*>();

	NodeRecord endNodeRecord = NodeRecord();
	NodeRecord current = NodeRecord();

	float endNodeHeuristic;

	NodeRecord startRecord = NodeRecord();
	startRecord.node = start;
	startRecord.connection = nullptr;
	startRecord.costSoFar = 0;
	startRecord.estimatedTotalCost = euclidean_heuristic(start, goal); // Heuristic

	nset open = nset(cmp_ptr());
	node_map openMap = node_map();
	node_map closedMap = node_map();
	open.insert(startRecord);
	openMap[start] = startRecord; // Node saved on map and pqueue
	while (!open.empty()) {
		current = *open.begin();
		open.erase(*open.begin());

		Node* currentnode = current.node;
		if (currentnode == goal) break;

		for (auto connection : current.node->connections) {
			Node * endNode = connection->toNode;
			float endNodeCost = current.costSoFar + connection->cost;
			NodeRecord gotclosed = closedMap[endNode];
			NodeRecord gotopen = openMap[endNode];
			if (!(gotclosed == NodeRecord())) {
				endNodeRecord = gotclosed;
				if (endNodeRecord.costSoFar <= endNodeCost) continue;
				closedMap[endNode] = NodeRecord();
				endNodeHeuristic = endNodeCost - endNodeRecord.costSoFar;
			}
			else if (!(gotopen == NodeRecord())) {
				endNodeRecord = gotopen;
				if (endNodeRecord.costSoFar <= endNodeCost) continue;
				endNodeHeuristic = endNodeCost - endNodeRecord.costSoFar;
			} 
			else { //Unvisited node
				endNodeRecord = NodeRecord();
				endNodeRecord.node = endNode;
				endNodeHeuristic = euclidean_heuristic(endNode, goal);
			}
			endNodeRecord.costSoFar = endNodeCost;
			endNodeRecord.connection = connection;
			endNodeRecord.estimatedTotalCost = endNodeCost + endNodeHeuristic;

			if (gotopen == NodeRecord()) {
				open.insert(endNodeRecord);
				openMap[endNode] = endNodeRecord;
			}
		}
		// connections finished for current node
		openMap[currentnode] = NodeRecord();
		closedMap[currentnode] = current;
	}
	if (current.node != goal) {
		std::cout << "Unreachable" << std::endl;
	}
	else {
		while (current.node != start) {
			path.insert(path.begin(), current.connection);
			current = closedMap[current.connection->fromNode];
		}
	}
	return path;
};


glm::vec3 getPos(Vertex vertex, glm::vec3 position, glm::vec3 size) {
	return (vertex.Position + position)*size;
}

Graph::Graph(Model* model, glm::vec3 position, glm::vec3 size) {
	std::unordered_map<std::string, std::unordered_set<Node*>> adjacencyMap;

	for (auto mesh : model->meshes) {
		string str = mesh.name.data;
		if (str == Globals::map_name) {
			for (size_t i = 0; i < mesh.indices.size(); i += 3) {
				size_t index = mesh.indices[i];
				size_t index2 = mesh.indices[i + 1];
				size_t index3 = mesh.indices[i + 2];
				Vertex vertices = mesh.vertices[index];
				Vertex vertices2 = mesh.vertices[index2];
				Vertex vertices3 = mesh.vertices[index3];

				// Points up
				if (glm::angle(vertices.Normal, glm::vec3(0.0f, 1.0f, 0.0f)) < 1) {
					glm::vec3 Pos1 = getPos(vertices, position, size), Pos2 = getPos(vertices2, position, size), Pos3 = getPos(vertices3, position, size);
					Node* node = new Node(Pos1, Pos2, Pos3, vertices.Normal);
					nodes.push_back(node);
					//Compare by triangles sides
					adjacencyMap[to_string(Pos3) + to_string(Pos1)].insert(node);
					adjacencyMap[to_string(Pos1) + to_string(Pos3)].insert(node);

					adjacencyMap[to_string(Pos1) + to_string(Pos2)].insert(node);
					adjacencyMap[to_string(Pos2) + to_string(Pos1)].insert(node);

					adjacencyMap[to_string(Pos3) + to_string(Pos2)].insert(node);
					adjacencyMap[to_string(Pos2) + to_string(Pos3)].insert(node);
				}

			}
		}
	}
	for (auto node : nodes) {
		for (auto adjnode : adjacencyMap[to_string(node->p1) + to_string(node->p2)]) {
			if (!(*adjnode == *node)) {
				node->connections.push_back(new Connection(glm::length(node->center() - adjnode->center()), node, adjnode));
			}
		}
		for (auto adjnode : adjacencyMap[to_string(node->p2) + to_string(node->p3)]) {
			if (!(*adjnode == *node)) {
				node->connections.push_back(new Connection(glm::length(node->center() - adjnode->center()), node, adjnode));
			}
		}
		for (auto adjnode : adjacencyMap[to_string(node->p3) + to_string(node->p1)]) {
			if (!(*adjnode == *node)) {
				node->connections.push_back(new Connection(glm::length(node->center() - adjnode->center()), node, adjnode));
			}
		}
	}
	// setting connection lines to draw
	glGenVertexArrays(2, VAO);
	glGenBuffers(2, VBO);
	std::vector<glm::vec3> vertices, connections;

	for (auto node : nodes) {
		vertices.push_back(node->p1);
		vertices.push_back(node->p2);
		vertices.push_back(node->p2);
		vertices.push_back(node->p3);
		vertices.push_back(node->p3);
		vertices.push_back(node->p1);
		for (auto adj : node->connections) {
			connections.push_back(node->center());
			connections.push_back(adj->toNode->center());
		}
	}

	// connections
	glBindVertexArray(VAO[0]);
	glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0);

	glBindVertexArray(VAO[1]);
	glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
	glBufferData(GL_ARRAY_BUFFER, connections.size() * sizeof(glm::vec3), &connections[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void*)0);

	glBindVertexArray(0);
	num_vertices = vertices.size();
	num_connections = connections.size();
}

bool checkNodePosition(glm::vec3 position, Node* node) {
	if (node) return glm::intersectRayTriangle(position, glm::vec3(0.0f, -1.0f, 0.0f), node->p1, node->p2, node->p3, glm::vec3());
	return false;
}

Node* Graph::getNode(glm::vec3 position) {
	for (auto node : nodes) {
		if (checkNodePosition(position, node)) return node;
	}
	return nullptr;
}
Node* Graph::getNode(glm::vec3 position, Node* previous) {
	// Check if its in the same node
	if (!previous) return getNode(position);
	if (checkNodePosition(position, previous)) return previous;
	/* Perform bfs from the inital node*/
	for (auto adj : previous->connections) {
		Node* node = adj->toNode;
		if (checkNodePosition(position, node)) return node;
	}
	return nullptr;
}

void Graph::draw(Shader shader) {
	shader.Use();
	glm::mat4 model;
	shader.setVec4("ourColor", glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
	shader.setMat4("model", model);

	glLineWidth(5.0f);
	glBindVertexArray(this->VAO[0]);
	glDrawArrays(GL_LINES, 0, num_vertices);

	shader.setVec4("ourColor", glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	glBindVertexArray(this->VAO[1]);
	glDrawArrays(GL_LINES, 0, num_connections);
	
	glLineWidth(1.0f);
	glBindVertexArray(0);
}