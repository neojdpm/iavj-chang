#ifndef GRAPH_H
#define GRAPH_H

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/intersect.hpp>
#include <glm/gtx/vector_angle.hpp>
#include "../model.h"

#include <set>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include <iostream>

//Node of a triangle mesh
class Node;
class Connection {
public:
	Connection(float cost, Node* fNode, Node* tNode) : cost(cost), fromNode(fNode), toNode(tNode) { }
	float cost;
	Node* fromNode;
	Node* toNode;
};

class Node
{
public:
	glm::vec3 p1;
	glm::vec3 p2;
	glm::vec3 p3;
	glm::vec3 normal;
	std::vector<Connection*> connections;

	Node(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 normal) :p1(p1), p2(p2), p3(p3), normal(normal){
	}

	glm::vec3 center() {
		return (p1 + p2 + p3) / 3.0f;
	}

	bool operator==(Node lhs) {
		return this->center() == lhs.center();
	}


};

class Graph
{
public:
	Mesh* mesh;
	std::vector<Node*> nodes;
	// Loads a map graph from a model
	Graph() { }
	Graph(Model* model, glm::vec3 position, glm::vec3 size);
	Node* getNode(glm::vec3 position);
	Node* getNode(glm::vec3 position, Node* previous); // Previous is the previous located node, to allocate the search
	void draw(Shader shader);
private:
	GLuint VAO[2], VBO[2];
	int num_vertices, num_connections;
};

bool checkNodePosition(glm::vec3 position, Node* node);
std::list<Connection*> pathFindAStar(Node* start, Node* end);

#endif