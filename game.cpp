#include "headers/game.h"

PathFollowing* splineBehaviour;
Model* ghostmodel, *ghostmodelboss, *mapModel, *doormodel;
Graph graph;
std::vector<Connection*> path;

DirLight dirLight = DirLight(glm::vec3(-0.4f, -1.0f, -0.4f), glm::vec3(0.01f, 0.01f, 0.01f)*2.0f, glm::vec3(0.10f, 0.10f, 0.15f)*2.0f, glm::vec3(0.05f, 0.05f, 0.05f));
PointLight pointLight = PointLight(glm::vec3(10.0f, 0.0f, -14.0f), glm::vec3(0.88f, 0.34f, 0.13f)*0.5f, 
							glm::vec3(0.88f, 0.34f, 0.13f), glm::vec3(0.88f, 0.34f, 0.13f)*0.1f, 1.0f, 0.14f, 0.07f);

PointLight* pointLights[] =
{
	new PointLight(glm::vec3(-7.42f, 1.2f, -10.21f), glm::vec3(0.88f, 0.34f, 0.13f),
	glm::vec3(0.88f, 0.34f, 0.13f), glm::vec3(0.88f, 0.34f, 0.13f)*0.1f, 1.0f, 0.14f, 0.07f),
	new PointLight(glm::vec3(-8.91f, 1.2f, -19.30f), glm::vec3(0.88f, 0.34f, 0.13f),
	glm::vec3(0.88f, 0.34f, 0.13f), glm::vec3(0.88f, 0.34f, 0.13f)*0.1f, 1.0f, 0.14f, 0.07f),
	new PointLight(glm::vec3(-20.0f, 1.2f, 21.0f), glm::vec3(0.88f, 0.34f, 0.13f),
	glm::vec3(0.88f, 0.34f, 0.13f), glm::vec3(0.88f, 0.34f, 0.13f)*0.1f, 1.0f, 0.14f, 0.07f),
	new PointLight(glm::vec3(4.03f, 1.2f, 14.17f), glm::vec3(0.88f, 0.34f, 0.13f),
	glm::vec3(0.88f, 0.34f, 0.13f), glm::vec3(0.88f, 0.34f, 0.13f)*0.1f, 1.0f, 0.14f, 0.07f)
};

std::vector<PointLight*> pLights;

PostProcessor* postProcessor;

GameObject* world = new GameObject();

std::list<GameObject*> objectsMesh = std::list<GameObject*>();	// Every nesh in the game

Game::Game(GLuint width, GLuint height, float maxSpeed, float maxAcceleration) : Width(width), Height(height), maxSpeed(maxSpeed), maxAcceleration(maxAcceleration){
};

Game::~Game() { };
	// Initialize game state (load all shaders/textures/levels)
void Game::setCamera(Camera* camera) {
		this->camera = camera;
	}

void Game::Init() {
	srand(time(NULL));
	// Load shaders
	ResourceManager::LoadShader("shaders/Model/Flame/shader.vs", "shaders/Model/Flame/shader.fs", nullptr, "box");
	ResourceManager::LoadShader("shaders/DepthShader/shader.vs", "shaders/DepthShader/shader.fs", nullptr, "depthShader");
	ResourceManager::LoadShader("shaders/DepthCubeShader/shader.vs", "shaders/DepthCubeShader/shader.fs", "shaders/DepthCubeShader/shader.gs", "depthCubeShader");
	ResourceManager::LoadShader("shaders/Splines/shader.vs", "shaders/Splines/shader.fs", nullptr, "lines");
	ResourceManager::LoadShader("shaders/Model/shader.vs", "shaders/Model/WireFraming/shader.fs", "shaders/Model/WireFraming/shader.gs", "wireframe");
	ResourceManager::LoadShader("shaders/Model/shader.vs", "shaders/Model/shader.fs", nullptr, "model");
	ResourceManager::LoadShader("shaders/GodRays/shader.vs", "shaders/GodRays/shader.fs", nullptr, "godrays");
	Debug::Shaders["lines"] = ResourceManager::GetShader("lines");
	Debug::SetupBuffers();

	// Load textures
	ResourceManager::LoadTexture("resources/container.jpg", GL_FALSE, "box");
	ResourceManager::LoadTexture("resources/ParticleFlamesSheetA.png", GL_TRUE, "flamesheet");
	// PostProcessor
	postProcessor = new PostProcessor(ResourceManager::GetShader("godrays"), this->Width, this->Height);

	dirLight.setShadow();
	//pointLight.setShadow();
	for (auto p : pointLights) {
		p->setShadow();
		pLights.push_back(p);
	}
	//pLights.push_back(&pointLight);

	/* Setting model shader properties */
	Shader shader = ResourceManager::GetShader("model").Use();

	shader.setInt("shadowMap", 9);
	// setting shadow shaders 
	shader.setInt("shadowCubeMap", 10);
	for (int i = 1; i < 10; i++)
		shader.setInt("shadowCubeMap" + std::to_string(i), 10 + i);

	// Directional light properties
	shader.setVec3("dirLight.direction", dirLight.Direction);
	shader.setVec3("dirLight.ambient", dirLight.Ambient);
	shader.setVec3("dirLight.diffuse", dirLight.Diffuse);
	shader.setVec3("dirLight.specular", dirLight.Specular);
	// set Point Lights
	int i = 0;
	for (auto pL : pLights) {
		shader.setVec3("pointLights[" + std::to_string(i) + "].position", pL->Position);
		shader.setVec3("pointLights[" + std::to_string(i) + "].ambient", pL->Ambient);
		shader.setVec3("pointLights[" + std::to_string(i) + "].diffuse", pL->Diffuse);
		shader.setVec3("pointLights[" + std::to_string(i) + "].specular", pL->Specular);
		shader.setFloat("pointLights[" + std::to_string(i) + "].constant", pL->constant);
		shader.setFloat("pointLights[" + std::to_string(i) + "].linear", pL->linear);
		shader.setFloat("pointLights[" + std::to_string(i) + "].quadratic", pL->quadratic);
		i++;
	}
	shader.setInt("num_point_lights", pLights.size());

	/****** END LIGHTS  ******/

	doormodel = new Model("models/Objects/GO/Door/Door.obj");
	ghostmodel = new Model("models/Objects/Characters/Dad/NewDad.obj");
	mapModel = new Model("models/Map/TestMap2.obj");
	//ghostmodel = new Model("models/ns/nanosuit.obj");
	ghostmodelboss = new Model("models/Objects/Characters/Ghost/GhostSanta.obj");

	// Map
	world->kinematic.Position = glm::vec3(0.0f);
	world->setModel(mapModel);
	world->Size = glm::vec3(2.0f);
	world->model->setShader(ResourceManager::GetShader("model"));

	graph = Graph(world->model, world->kinematic.Position, world->Size);
	// Generate and load Meshes
	QuadMesh* quadMesh = new QuadMesh(ResourceManager::GetShader("box"));
	ResourceManager::LoadMesh(quadMesh, "flamesheet");
	quadMesh->setTexture(ResourceManager::GetTexture("flamesheet"));

	CubeMesh* boxMesh = new CubeMesh(ResourceManager::GetShader("box"));
	ResourceManager::LoadMesh(boxMesh, "box");
	boxMesh->setTexture(ResourceManager::GetTexture("box"));

	CubeMesh* wallMesh = new CubeMesh(ResourceManager::GetShader("box"));
	ResourceManager::LoadMesh(wallMesh, "box");
	wallMesh->setTexture(ResourceManager::GetTexture("box"));

	// Flames
	GameObject* flame = new GameObject();
	flame->kinematic.Position = glm::vec3(-7.42f, 1.6f, -10.5f);
	pointLights[0]->Position = flame->kinematic.Position;
	pointLights[0]->Position.y += 1.0f;
	flame->Size = glm::vec3(0.15f);
	flame->setMesh(quadMesh);
	objectsMesh.push_back(flame);

	GameObject* flame2 = new GameObject();
	flame2->kinematic.Position = glm::vec3(-8.91f, 1.6f, -19.50f);
	pointLights[1]->Position = flame2->kinematic.Position;
	pointLights[1]->Position.y += 1.0f;
	flame2->Size = glm::vec3(0.15f);
	flame2->setMesh(quadMesh);
	objectsMesh.push_back(flame2);

	GameObject* flame3 = new GameObject();
	flame3->kinematic.Position = glm::vec3(-20.0f, 1.6f, 21.0f);
	pointLights[2]->Position = flame3->kinematic.Position;
	pointLights[2]->Position.y += 1.0f;
	flame3->Size = glm::vec3(0.15f);
	flame3->setMesh(quadMesh);
	objectsMesh.push_back(flame3);

	GameObject* flame4 = new GameObject();
	flame4->kinematic.Position = glm::vec3(4.03f, 1.6f, 14.0f);
	pointLights[3]->Position = flame4->kinematic.Position;
	pointLights[3]->Position.y += 1.0f;
	flame4->Size = glm::vec3(0.15f);
	flame4->setMesh(quadMesh);
	objectsMesh.push_back(flame4);
	//
	// Players
	player = new GameObject();
	player->setModel(ghostmodel);
	player->kinematic.Position = glm::vec3(1.0f, 0.0f, 0.8f);
	std::list<GameObject*> obstacles = std::list<GameObject*>();	// Every object in the game
	obstacles.push_back(world);
	this->objects.push_back(player);
	this->players.push_back(player);

	// Enemy 1
	GameObject* ghosts = new GameObject[30]();

	GameObject* wall1 = new GameObject();
	wall1->kinematic.Position = glm::vec3(10.0f, 0.0f, -17.0f);
	wall1->setModel(ghostmodelboss);
	wall1->Size = glm::vec3(2.5f);
	this->players.push_back(wall1);
	this->objects.push_back(wall1);

	GameObject* wall2 = new GameObject();
	wall2->setModel(ghostmodelboss);
	wall2->kinematic.Position = glm::vec3(5.0f, 0.0f, -5.0f);
	wall2->Size = glm::vec3(2.5f);
	this->players.push_back(wall2);
	this->objects.push_back(wall2);

	GameObject* wall3 = new GameObject();
	wall3->setModel(ghostmodelboss);
	wall3->kinematic.Position = glm::vec3(5.0f, 0.0f, -7.0f);
	wall3->Size = glm::vec3(2.5f);
	this->players.push_back(wall3);
	this->objects.push_back(wall3);

	this->objects.push_back(world);

	// GHOST 1
	std::list<State*> states = std::list<State*>();
	State* pursue1 = new State();
	State* wander1 = new State();
	DetectedCondition* seen = new DetectedCondition(wall1, player, 8.0f, 0);
	Transition toPursue = Transition(pursue1, seen);
	wander1->addTransition(toPursue);
	wander1->addAction(new SeekPathingRandomized(wall1, &graph, 2.5f, 0.5f, 5.0f, 4.0f, 0.2f, 1.0f, 0.1f));
	wander1->addAction(new ObstacleAvoidance(wall1, obstacles, 10.0f, 2.0f, 10.0f));
	wander1->addAction(new LookWhereYouAreGoing(wall1, 15.0f, 6.0f, 2.0f, 5.0f, 0.1f));
	states.push_back(wander1);

	DetectedCondition* unseen = new DetectedCondition(wall1, player, 120.0f, 1);
	Transition toWander = Transition(wander1, unseen);
	pursue1->addTransition(toWander);
	pursue1->addAction(new SeekPathing(wall1, player, &graph, 3.5f, 0.35f, 5.0f, 4.0f, 0.2f, 1.0f, 0.1f));
	pursue1->addAction(new ObstacleAvoidance(wall1, obstacles, 10.0f, 2.0f, 10.0f));
	pursue1->addAction(new LookWhereYouAreGoing(wall1, 15.0f, 6.0f, 2.0f, 5.0f, 0.1f));
	states.push_back(pursue1);

	wall1->machine = StateMachine(*pursue1, states);

	// GHOST 2
	std::list<State*> states2 = std::list<State*>();
	State* pursue2 = new State();
	State* wander2 = new State();
	DetectedCondition* seen2 = new DetectedCondition(wall2, player, 5.0f, 0);
	Transition toPursue2 = Transition(pursue2, seen2);
	wander2->addTransition(toPursue2);
	wander2->addAction(new SeekPathingRandomized(wall2, &graph, 2.5f, 0.5f, 5.0f, 4.0f, 0.2f, 1.0f, 0.1f));
	wander2->addAction(new ObstacleAvoidance(wall2, obstacles, 10.0f, 2.0f, 10.0f));
	wander2->addAction(new LookWhereYouAreGoing(wall2, 15.0f, 6.0f, 2.0f, 5.0f, 0.1f));
	states2.push_back(wander2);

	DetectedCondition* unseen2 = new DetectedCondition(wall2, player, 120.0f, 1);
	Transition toWander2 = Transition(wander2, unseen2);
	pursue2->addTransition(toWander2);
	pursue2->addAction(new SeekPathing(wall2, player, &graph, 3.5f, 0.35f, 5.0f, 4.0f, 0.2f, 1.0f, 0.1f));
	pursue2->addAction(new ObstacleAvoidance(wall2, obstacles, 10.0f, 2.0f, 10.0f));
	pursue2->addAction(new LookWhereYouAreGoing(wall2, 15.0f, 6.0f, 2.0f, 5.0f, 0.1f));
	states2.push_back(pursue2);

	wall2->machine = StateMachine(*pursue2, states2);

	// GHOST 3
	std::list<State*> states3 = std::list<State*>();
	State* pursue3 = new State();
	State* wander3 = new State();
	DetectedCondition* seen3 = new DetectedCondition(wall3, player, 5.0f, 0);
	Transition toPursue3 = Transition(pursue3, seen3);
	wander3->addTransition(toPursue3);
	wander3->addAction(new SeekPathingRandomized(wall3, &graph, 2.5f, 0.5f, 5.0f, 4.0f, 0.2f, 1.0f, 0.1f));
	wander3->addAction(new ObstacleAvoidance(wall3, obstacles, 10.0f, 2.0f, 10.0f));
	wander3->addAction(new LookWhereYouAreGoing(wall3, 15.0f, 6.0f, 2.0f, 5.0f, 0.1f));
	states3.push_back(wander3);

	DetectedCondition* unseen3 = new DetectedCondition(wall3, player, 120.0f, 1);
	Transition toWander3 = Transition(wander3, unseen3);
	pursue3->addTransition(toWander3);
	pursue3->addAction(new SeekPathing(wall3, player, &graph, 3.5f, 0.35f, 5.0f, 4.0f, 0.2f, 1.0f, 0.1f));
	pursue3->addAction(new ObstacleAvoidance(wall3, obstacles, 10.0f, 2.0f, 10.0f));
	pursue3->addAction(new LookWhereYouAreGoing(wall3, 15.0f, 6.0f, 2.0f, 5.0f, 0.1f));
	states3.push_back(pursue3);
	wall3->machine = StateMachine(*pursue3, states3);



	/*
	//Wander* behaviour2 = new Wander(player, 0.0f, 5.0f, 0.5f, 0.0f, 10.0f, 1.0f, 1.0f, 2.0f, 10.0f, 0.2f);
	//DynamicPursue* behaviour2 = new DynamicPursue(player, enemy, 1.0f, 2.0f);
	//DynamicArrive* behaviour2 = new DynamicArrive(player, enemy, 1.0f, 10.0f, 1.5f, 4.0f, 0.1f);
	//Align* behaviour2 = new Align(player, enemy, 2.0f, 5.0f, 3.0f, 10.0f, 0.1f);
	//Face* behaviour2 = new Face(player, enemy, 1.0f, 3.0f, 1.0f, 10.0f, 0.05f);*/

	/* obstacles */

	//splineBehaviour = new PathFollowing(player, spline, 3.0f, 0.015f, 3.0f, 2.0f, 0.2f, 1.0f, 0.1f);

	//player->addBehaviour(new Align(player, wall1, 5.0f, 3.0f, 3.0f, 20.0f, 0.1f));
	std::list<State*> playerStates = std::list<State*>();
	State* playerState = new State();
	//playerState->addAction(new SeekPathing(wall1, player, &graph, 3.5f, 0.35f, 5.0f, 4.0f, 0.2f, 1.0f, 0.1f));
	playerState->addAction(new ObstacleAvoidance(player, obstacles, 10.0f, 2.0f, 10.0f));
	playerState->addAction(new LookWhereYouAreGoing(player, 15.0f, 6.0f, 2.0f, 5.0f, 0.1f));
	playerStates.push_back(playerState);
	player->machine = StateMachine(*playerState, playerStates);
	//player->addBehaviour(new ObstacleAvoidance(player, obstacles, 10.0f, 2.0f, 10.0f));
}

// GameLoop
void Game::Update(GLfloat dt) {
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	/*float oldY = camera->Position.y;
	camera->Position = player->kinematic.Position;
	camera->Position.y = oldY;*/

	// Camera 
	glm::mat4 projection = glm::perspective(glm::radians(camera->Zoom), (float)Width / (float)Height, 0.1f, 100.0f);
	ResourceManager::GetShader("box").Use().setMat4("projection", projection);
	ResourceManager::GetShader("lines").Use().setMat4("projection", projection);
	ResourceManager::GetShader("model").Use().setMat4("projection", projection);
	ResourceManager::GetShader("wireframe").Use().setMat4("projection", projection);

	// camera/view transformation
	glm::mat4 view = camera->GetViewMatrix();
	ResourceManager::GetShader("box").Use().setMat4("view", view);
	ResourceManager::GetShader("lines").Use().setMat4("view", view);
	ResourceManager::GetShader("model").Use().setMat4("view", view);
	ResourceManager::GetShader("wireframe").Use().setMat4("view", view);

	// Setting camera/viewpos
	ResourceManager::GetShader("model").Use().setVec3("viewPos", camera->Position);
	
	// Setting camera spotlight 
	ResourceManager::GetShader("model").setVec3("spotLight.position", camera->Position);
	ResourceManager::GetShader("model").setVec3("spotLight.direction", camera->Front);

	/* Num Textures to 20*/
	ResourceManager::GetShader("model").Use().setInt("numTex", 20);
	/* Pseudo blinking effect */
	float sen = sin(10.0f * glfwGetTime());

	for (auto p : pointLights) {
		p->Ambient = glm::vec3(0.88f, 0.34f, 0.13f)*0.5f*(1 - 0.1f*sen*randomBinomial());
		p->Diffuse = glm::vec3(0.88f, 0.34f, 0.13f)*(1 - 0.1f*sen*randomBinomial());
	}

	camera->Position = glm::vec3(player->kinematic.Position.x, camera->Position.y, player->kinematic.Position.z);
	glm::vec4 viewport = glm::vec4(0.0f, 0.0f, Globals::SCR_WIDTH, Globals::SCR_HEIGHT);
	glm::mat4 model;
	glm::vec3 p = glm::project(glm::vec3(0.0f), view * model,  projection, viewport);

	//std::cout << p.x / Globals::SCR_WIDTH << " " << p.y / Globals::SCR_HEIGHT << std::endl;
}

void Game::Render() {

	/* DRAW THE SCENE SHADOW MAP FIRST */
	glm::mat4 lightProjection, lightView, lightSpaceMatrix;
	float near_plane = 1.0f, far_plane = 100.0f;
	lightProjection = glm::ortho(-35.0f, 35.0f, -35.0f, 35.0f, near_plane, far_plane);
	lightView = glm::lookAt(glm::vec3(0.4f, 1.0f, 0.4f)*20.0f, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
	lightSpaceMatrix = lightProjection * lightView;

	Shader* simpleDepthShader = &ResourceManager::GetShader("depthShader").Use();
	simpleDepthShader->setMat4("lightSpaceMatrix", lightSpaceMatrix);
	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, dirLight.depthMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);

	glDisable(GL_CULL_FACE);
	for (GameObject* i : this->objects) {
		i->Draw(simpleDepthShader, glm::vec3(0.5f,0.5f,0.5f));
	}
	glEnable(GL_CULL_FACE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// 2. render scene to EACH POINT LIGHT depth cubemap
	// --------------------------------
	Shader* simpleDepthCubeShader = &ResourceManager::GetShader("depthCubeShader").Use();
	int i = 0;
	for (auto pL : pLights) {
		std::vector<glm::mat4> shadowTransforms = pL->getShadowMatrix();
		glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
		glBindFramebuffer(GL_FRAMEBUFFER, pL->depthCubeMapFBO);
		glClear(GL_DEPTH_BUFFER_BIT);
		for (unsigned int i = 0; i < 6; ++i)
			simpleDepthCubeShader->setMat4("shadowMatrices[" + std::to_string(i) + "]", shadowTransforms[i]);
		simpleDepthCubeShader->setFloat("far_plane", 25.0f);
		simpleDepthCubeShader->setVec3("lightPos", pL->Position);
		glCullFace(GL_FRONT);
		for (GameObject* i : this->objects) {
			i->Draw(simpleDepthCubeShader, glm::vec3(0.5f, 0.5f, 0.5f));
		}
		glCullFace(GL_BACK);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		i++;
	}

	Shader * shader;
	if (Globals::triangles) shader = &ResourceManager::GetShader("wireframe").Use();
	else shader = &ResourceManager::GetShader("model").Use();
	i = 0;
	for (auto pL : pLights) { // setting positions and binding textures
		shader->setVec3("pointLights[" + std::to_string(i) + "].position", pL->Position);
		shader->setVec3("pointLights[" + std::to_string(i) + "].ambient", pL->Ambient);
		shader->setVec3("pointLights[" + std::to_string(i) + "].diffuse", pL->Diffuse);
		glActiveTexture(GL_TEXTURE10 + i);
		glBindTexture(GL_TEXTURE_CUBE_MAP, pL->depthCubeMap);
		i++;
	}

	/* THEN DRAW THE SCENE WITH THE SHADOW DEPTH MAP*/
	glViewport(0, 0, Globals::SCR_WIDTH, Globals::SCR_HEIGHT);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	shader->setMat4("lightSpaceMatrix", lightSpaceMatrix);
	shader->setFloat("far_plane", 25.0f);
	glActiveTexture(GL_TEXTURE9);
	glBindTexture(GL_TEXTURE_2D, dirLight.depthMap);
	for (GameObject* i : this->objects) {
		i->Draw(shader, glm::vec3(0.5f, 0.5f, 0.5f));
	}

	glDisable(GL_CULL_FACE);
	for (GameObject* i : objectsMesh) {

		i->Draw(shader, glm::vec3(0.5f, 0.5f, 0.5f));
		i->mesh->setTime(glfwGetTime());
	}
	glEnable(GL_CULL_FACE);

	if (Globals::drawGraph) {
		graph.draw(ResourceManager::GetShader("lines"));
	}

}

void Game::PhysicsLoop(GLfloat dt) {
	for (GameObject* i : this->players) {
		i->getSteering();
		i->kinematic.Position += i->kinematic.Velocity * dt;
		i->kinematic.Orientation += i->kinematic.Rotation * dt;

		i->kinematic.Velocity += i->steering.Linear * dt + Globals::gravity*dt;
		i->kinematic.Rotation += i->steering.Angular * dt;

		calculateCollisionFloor(&graph, i, 0.3f);
		//if (i == player) calculateCollisionWall(player, 1.0f, objects);
		// Clip to max speed
		if (glm::abs(glm::length(i->kinematic.Velocity)) > this->maxSpeed) {
			i->kinematic.Velocity = glm::normalize(i->kinematic.Velocity) * maxSpeed;
		}
		// drag
		if (i == player) i->kinematic.Velocity = glm::mix(i->kinematic.Velocity, i->kinematic.Velocity*0.5f, 0.03f);
	}
}

void Game::DrawPoint(float x, float y) {
	glm::vec4 worldPos = camera->GetWorldPos(x, y, Width, Height);
	std::cout << to_string(worldPos) << std::endl;

	if (splineBehaviour) {

		if (splineBehaviour->path->size > 3) {
			splineBehaviour->path->popPoint3();
		}

		splineBehaviour->path->addPoint3(worldPos.x, 0.0f, worldPos.z);
		splineBehaviour->UpdatePath();
		splineBehaviour->path->updateRenderData();
	}
}

void Game::ProcessMovement(Camera_Movement direction, float deltaTime)
{
	float velocity = 5 * deltaTime;
	if (direction == FORWARD)
		player->kinematic.Velocity += glm::vec3(0.0f, 0.0f, -1.0f)* velocity;
	if (direction == BACKWARD)
		player->kinematic.Velocity -= glm::vec3(0.0f, 0.0f, -1.0f)* velocity;
	if (direction == LEFT)
		player->kinematic.Velocity -= glm::vec3(1.0f, 0.0f, 0.0f)* velocity;
	if (direction == RIGHT)
		player->kinematic.Velocity += glm::vec3(1.0f, 0.0f, 0.0f)* velocity;
	/*
	if (direction == FORWARD)
		player->kinematic.Position += glm::vec3(0.0f, 0.0f, -1.0f) * velocity;
	if (direction == BACKWARD)
		player->kinematic.Position -= glm::vec3(0.0f, 0.0f, -1.0f)  * velocity;
	if (direction == LEFT)
		player->kinematic.Position -= glm::vec3(1.0f, 0.0f, 0.0f) * velocity;
	if (direction == RIGHT)
		player->kinematic.Position += glm::vec3(1.0f, 0.0f, 0.0f) * velocity;
		*/

}

void Game::ProcessJump()
{
	if (!player->jumping) player->jump(2.0f);

}

