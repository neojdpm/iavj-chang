#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;

out vec3 FragPos;
out vec3 Normal;
out vec2 TexCoords;
out vec4 FragPosLightSpace;   

uniform int frame = 0; // from 0 to 31
uniform int numTex = 1;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 lightSpaceMatrix;

void main()
{
	FragPos = vec3(model * vec4(aPos, 1.0));
    Normal = mat3(transpose(inverse(model))) * aNormal;  

	//Frame Calculation
	float frame_x = mod(frame, 8.0f);
	float frame_y = floor(frame/8.0f);
	vec2 Coords = aTexCoords;
	Coords.x = Coords.x + frame_x * 0.125f;
	Coords.y = Coords.y - frame_y * 0.25f;
    TexCoords = numTex * Coords; 


    FragPosLightSpace = lightSpaceMatrix * vec4(FragPos, 1.0);
    gl_Position = projection * view * model * vec4(aPos, 1.0);
}