#version 330 core
out vec4 FragColor;

struct Material {
    vec3 diffuse;
    vec3 specular;  
    float opacity;  
    float shininess;
}; 

struct DirLight {
    vec3 direction;
	
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct PointLight {
    vec3 position;
    
    float constant;
    float linear;
    float quadratic;
	
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct SpotLight {
    vec3 position;
    vec3 direction;
    float cutOff;
    float outerCutOff;
  
    float constant;
    float linear;
    float quadratic;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;       
};

#define NR_POINT_LIGHTS 10
in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;
in vec4 FragPosLightSpace;


uniform sampler2D shadowMap;
uniform sampler2D texture_normal1;
// LIGHTS PRE OPENGL4
uniform samplerCube shadowCubeMap;
uniform samplerCube shadowCubeMap1;
uniform samplerCube shadowCubeMap2;
uniform samplerCube shadowCubeMap3;
uniform samplerCube shadowCubeMap4;
uniform samplerCube shadowCubeMap5;
uniform samplerCube shadowCubeMap6;
uniform samplerCube shadowCubeMap7;
uniform samplerCube shadowCubeMap8;
uniform samplerCube shadowCubeMap9;

uniform float far_plane;
uniform int num_point_lights;
uniform bool is_texture = false;
uniform bool is_ghost = false;
uniform vec3 viewPos;
uniform DirLight dirLight;
uniform PointLight pointLights[NR_POINT_LIGHTS];
uniform SpotLight spotLight;
uniform Material material;

vec3 pointLightsValues[NR_POINT_LIGHTS];

// function prototypes
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir, float shadow);
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir, int i);
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir);

samplerCube getMapCube(int i) {
	return samplerCube[](shadowCubeMap, shadowCubeMap1, shadowCubeMap2, shadowCubeMap3, shadowCubeMap4, 
														shadowCubeMap5, shadowCubeMap6, shadowCubeMap7, shadowCubeMap8, shadowCubeMap9)[i];
}

float ShadowCalculation(vec3 normal, vec4 fragPosLightSpace)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadowMap, projCoords.xy).r; 
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // check whether current frag pos is in shadow
	// BIAS setting (CAUSING DISPLACEMENT) float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);  
	
	// PCF
    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r; 
            shadow += currentDepth - 0.0005> pcfDepth  ? 1.0 : 0.0;        
        }    
    }
    shadow /= 9.0;

    if(projCoords.z > 1.0)
        shadow = 0.0;

    return shadow;
}

float ShadowCubeCalculation(vec3 lightPos, vec3 fragPos, int i)
{
    // get vector between fragment position and light position
    vec3 fragToLight = fragPos - lightPos;
    // ise the fragment to light vector to sample from the depth map     
    float closestDepth = texture(getMapCube(i), fragToLight).r;
    // it is currently in linear range between [0,1], let's re-transform it back to original depth value
    closestDepth *= far_plane;
    // now get current linear depth as the length between the fragment and light position
    float currentDepth = length(fragToLight);
    // test for shadows
    float bias = 0.05; // we use a much larger bias since depth is now in [near_plane, far_plane] range
    float shadow = currentDepth > closestDepth ? 1.0 : 0.0;        
    // display closestDepth as debug (to visualize depth cubemap)
    // FragColor = vec4(vec3(closestDepth / far_plane), 1.0);    
        
    return shadow;
}

void main()
{    
	vec3 result;
	if (!is_ghost) {
		// properties
		vec3 norm = normalize(Normal);
		vec3 viewDir = normalize(viewPos - FragPos);
    
		// == =====================================================
		// Our lighting is set up in 3 phases: directional, point lights and an optional flashlight
		// For each phase, a calculate function is defined that calculates the corresponding color
		// per lamp. In the main() function we take all the calculated colors and sum them up for
		// this fragment's final color.
		// == =====================================================
	
		float shadow = ShadowCalculation(norm, FragPosLightSpace); 
		// phase 2: point lights (my phase 1)
		for(int i = 0; i < num_point_lights; i++) {
			pointLightsValues[i] = CalcPointLight(pointLights[i], norm, FragPos, viewDir, i);
			result += pointLightsValues[i];   
		}
		// phase 1: directional lighting (my phase 2)
		result += CalcDirLight(dirLight, norm, viewDir, shadow);
		// phase 3: spot light
		//result += CalcSpotLight(spotLight, norm, FragPos, viewDir);    
	}
    else result = material.diffuse;
    
    FragColor = vec4(result, material.opacity);
}

// calculates the color when using a directional light.
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir, float shadow)
{
    vec3 lightDir = normalize(-light.direction);
    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // specular shading
	// BLING
	vec3 halfwayDir = normalize(lightDir + viewDir);  
    float spec = pow(max(dot(normal, halfwayDir), 0.0), material.shininess);
    // PHONG
 	//vec3 reflectDir = reflect(-lightDir, normal);
    //float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    // combine results
	vec3 ambient, diffuse, specular;
	if (is_texture) {
		ambient = light.ambient * texture(texture_normal1, TexCoords).rgb;
		diffuse = light.diffuse * diff * texture(texture_normal1, TexCoords).rgb;
		specular = light.specular * spec * material.specular;
	}
	else {
		ambient = light.ambient * material.diffuse;
		diffuse = light.diffuse * diff * material.diffuse;
		specular = light.specular * spec * material.specular;
	}
	// Check if is lighted by a point light
	for(int i = 0; i < num_point_lights; i++) {
		PointLight pLight = pointLights[i];
		float shadowPoint = ShadowCubeCalculation(pLight.position, FragPos, i);
		if (length(pointLightsValues[i]) > length(light.ambient) && shadowPoint < 1.0) {
			float distance = length(pLight.position - FragPos);  
			float attenuation = 1.0 / (pLight.constant + pLight.linear * distance);    
			shadow = shadow * (1- attenuation); // shadow attenuation
		} 
	}

    return (ambient + (1.0 - shadow) * (diffuse + specular));
}

// calculates the color when using a point light.
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir, int i)
{
    vec3 lightDir = normalize(light.position - fragPos);
    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    // attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));    
    // combine results
	vec3 ambient, diffuse, specular;
	if (is_texture) {
		ambient = light.ambient * texture(texture_normal1, TexCoords).rgb;
		diffuse = light.diffuse * diff * texture(texture_normal1, TexCoords).rgb;
		specular = light.specular * spec * material.specular;
	}
	else {
		ambient = light.ambient * material.diffuse;
		diffuse = light.diffuse * diff * material.diffuse;
		specular = light.specular * spec * material.specular;
	}

    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;

    float shadow = ShadowCubeCalculation(light.position, fragPos, i);        
    return ( ambient + (1.0 - shadow) * (diffuse + specular));
}

// calculates the color when using a spot light.
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position - fragPos);
    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    // attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));    
    // spotlight intensity
    float theta = dot(lightDir, normalize(-light.direction)); 
    float epsilon = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    // combine results
    vec3 ambient = light.ambient * material.diffuse;
    vec3 diffuse = light.diffuse * diff * material.diffuse;
    vec3 specular = light.specular * spec * material.specular;
    ambient *= attenuation * intensity;
    diffuse *= attenuation * intensity;
    specular *= attenuation * intensity;
    return (ambient + diffuse + specular);
}
