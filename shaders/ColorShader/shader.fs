#version 330 core
out vec4 FragColor;

struct DirLight {
    vec3 direction;
	
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;
in vec4 FragPosLightSpace;


uniform sampler2D shadowMap;

uniform float far_plane;
uniform vec3 viewPos;
uniform DirLight dirLight;

// function prototypes
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir, float shadow);

float ShadowCalculation(vec3 normal, vec4 fragPosLightSpace)
{
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadowMap, projCoords.xy).r; 
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // check whether current frag pos is in shadow
	// BIAS setting (CAUSING DISPLACEMENT) float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);  
	
	// PCF
    float shadow = 0.0;
   
    shadow = currentDepth - 0.0005 > closestDepth  ? 1.0 : 0.0;        

    return shadow;
}


void main()
{    
    // properties
    vec3 norm = normalize(Normal);
    vec3 viewDir = normalize(viewPos - FragPos);
    
    float shadow = ShadowCalculation(norm, FragPosLightSpace); 

	vec3 result = CalcDirLight(dirLight, norm, viewDir, shadow);
    FragColor = vec4(result, 1.0);
}

// calculates the color when using a directional light.
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir, float shadow)
{
 	//vec3 reflectDir = reflect(-lightDir, normal);
    //float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    // combine results
	vec3 ambient, diffuse, specular;
	ambient = light.ambient;
	diffuse = light.diffuse;
	specular = light.specular;

    return ((1.0 - shadow) * vec3(ambient + diffuse + specular));
}

