﻿#version 330 core
out vec4 FragColor;

in vec3 FragPos;
in vec2 TexCoords;

// Our light scattering pass texture
uniform sampler2D scene;

void main()
{
	 FragColor = texture(scene, TexCoords);
}

void main2() {
	 vec2 lightPositionOnScreen = vec2(0.5, 0.0);
	 float decay=0.96815;
	 float exposure=0.2;
	 float density=0.926;
	 float weight=0.58767;
	 /// NUM_SAMPLES will describe the rays quality, you can play with
	 int NUM_SAMPLES = 200;
	 vec2 tc = TexCoords.xy;
	 vec2 deltaTexCoord = tc - lightPositionOnScreen.xt;

	 deltaTexCoord *= 1.0 / float(NUM_SAMPLES) * density;
	 float illuminationDecay = 1.0;
	 vec4 color =texture(scene, tc.xy)*0.4;
	 for(int i=0; i < NUM_SAMPLES ; i++)
	 {
		tc -= deltaTexCoord;
		vec4 sample = texture2D(scene, tc)*0.4;
		sample *= illuminationDecay * weight;
		color += sample;
		illuminationDecay *= decay;
	 }
	 FragColor = color * exposure; 
}