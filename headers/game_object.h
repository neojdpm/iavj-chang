#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glad/glad.h>
#include "../game_classes/model.h"
#include "../game_classes/Graph/graph.h"
#include <list>

class GameObject;
class State;
struct Kinematic {
	glm::vec3 Position, Velocity = glm::vec3();
	float Orientation, Rotation = 0.0f;
};

struct SteeringOutput {
	glm::vec3 Linear = glm::vec3();
	float Angular = 0.0f;
};

// STATE MACHINE CLASS
class Behaviour { //action
public:
	GameObject* character;
	virtual SteeringOutput getSteering() {
		return SteeringOutput();
	}
};

class Condition
{
public:
	virtual bool test() = 0;
};

class FloatContidion : public Condition
{
public:
	float minValue;
	float maxValue;

	float testValue;

	bool test() {
		return (minValue <= testValue && testValue <= maxValue);
	}
};

class AndCondition : public Condition
{
public:
	Condition* conditionA;
	Condition* conditionB;

	bool test() {
		return (conditionA->test() && conditionB->test());
	}
};

class NotCondition : public Condition
{
public:
	Condition* condition;

	bool test() {
		return (!condition->test());
	}
};

class OrCondition : public Condition
{
public:
	Condition* conditionA;
	Condition* conditionB;

	bool test() {
		return (conditionA->test() || conditionB->test());
	}
};


class Transition
{
public:
	std::list<Behaviour*> getAction() { return actions; }
	State* getTargetState() { return targetState; }
	bool isTriggered() { return condition->test(); }
	Transition(State* targetState, Condition* condition) : targetState(targetState), condition(condition) { };
private:
	std::list<Behaviour*> actions = std::list<Behaviour*>();
	State* targetState;
	Condition* condition;
};

class State
{
public:
	std::list<Behaviour*> actions;
	std::list<Behaviour*> entryactions;
	std::list<Behaviour*> exitactions;
	std::list<Transition> transitions;

	std::list<Behaviour*> getAction() { return actions; }
	std::list<Behaviour*> getEntryAction() { return entryactions; }
	std::list<Behaviour*> getExitAction() { return exitactions; }
	std::list<Transition> getTransitions() { return transitions; }
	void addAction(Behaviour* behaviour);
	void addTransition(Transition Transition);
};

class StateMachine
{
public:
	std::list<State*> states = std::list<State*>();
	State initial;
	State currentState;
	StateMachine() { };
	StateMachine(State initial, std::list<State*> states) : initial(initial), currentState(initial), states(states) { }

	std::list<Behaviour*> actions = std::list<Behaviour*>();
	std::list<Behaviour*> update();

};
// GAME OBJECT CLASS
class GameObject
{
public:
	// Object state
	Node* node;
	char*		   tag;
	Kinematic      kinematic;
	SteeringOutput steering;
	std::list<Behaviour*> behaviours = std::list<Behaviour*>();	// Every object in the game
	glm::vec3 Size = glm::vec3(1.0f, 1.0f, 1.0f);
	bool		alive = true;
	bool		jumping = false;
	bool		is_model = false;

	// Constructor(s)
	GameObject();
	GameObject(glm::vec3 pos, glm::vec3 velocity = glm::vec3(0.0f, 0.0f, 0.0f), GLfloat orientation = 0.0f, GLfloat rotation = 0.0f,
		glm::vec3 linear = glm::vec3(0.0f, 0.0f, 0.0f), GLfloat angular = 0.0f);

	void getSteering();
	void resetSteering();
	void setMesh(Mesh* mesh);
	void setModel(Model* model);
	void Draw(Shader* shader, glm::vec3 color);
	void Draw(glm::vec3 color);
	void addState(State* state);
	void jump(float intensity = 1.0f);
	Mesh* mesh;
	Model* model;
	StateMachine machine;
};

// Custom Conditions 
class DetectedCondition : public Condition
{
public:
	GameObject* character;
	GameObject* target;
	float range;
	short int mode; // 1 greater than, 0 less than

	DetectedCondition(GameObject* character, GameObject* target, float range, short int mode) 
		: character(character), target(target), range(range), mode(mode) {};
	bool test() {
		if (mode) {
			return (glm::length(character->kinematic.Position - target->kinematic.Position) >= range);
		} else return (glm::length(character->kinematic.Position - target->kinematic.Position) < range);
	}
};
#endif
