#ifndef GLOBALS_H
#define GLOBALS_H
#include "camera.h"
#include <string>
namespace Globals
{
	// forward declarations only
	extern unsigned int SCR_WIDTH;
	extern unsigned int SCR_HEIGHT;
	extern Camera* mycamera;
	extern bool triangles;
	extern bool drawGraph;
	extern bool paused;
	extern bool debugMode;
	extern unsigned int depthMap;
	extern unsigned int depthMapFBO;
	extern std::string map_name; 
	extern std::string walls_name;
	extern glm::vec3 gravity;
}
#endif